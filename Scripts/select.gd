extends Node2D

var streams = [preload("res://Sounds/select-up.wav"),preload("res://Sounds/select-down.wav"),preload("res://Sounds/satelite-appear.wav")]
@onready var audio: AudioStreamPlayer2D = get_node("Audio")
@onready var data:Data = get_node("/root/Data")

func _init():
	print("hello world")
	
var deltaPos = Vector2(0, 20)
var pos = 0

func _process(delta):
	position = deltaPos * pos

func _input(event):
	if event.is_action_pressed("up"):
		audio.stream = streams[0]
		audio.play()
		if (pos > 0):
			pos -= 1
	if event.is_action_pressed("down"):
		audio.stream = streams[1]
		audio.play()
		if (pos < 2):
			pos += 1
			
	if event.is_action_pressed("A") || event.is_action_pressed("start"):
		await get_tree().create_timer(0.2).timeout
		if (pos == 0): # new game
			data.setMusic(0)
			get_tree().change_scene_to_file("res://Scene/game.tscn")
		if (pos == 1): # score
			get_tree().change_scene_to_file("res://Scene/score.tscn")
		if (pos == 2): # credits
			get_tree().change_scene_to_file("res://Scene/credits.tscn")
