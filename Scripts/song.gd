extends AudioStreamPlayer2D

@onready var data:Data = get_node("/root/Data")
@onready var streamSong: AudioStreamWAV = preload("res://Sounds/song.wav")

# Called when the node enters the scene tree for the first time.
func _ready():
	streamSong.loop_mode = AudioStreamWAV.LOOP_DISABLED
	streamSong.mix_rate = 48000
	stream = streamSong
	play(data.musicVal)

func _exit_tree():
	data.setMusic(get_playback_position())

func _on_finished():
	play()
