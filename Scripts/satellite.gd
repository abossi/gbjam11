extends Node2D

@onready var data:Data = get_node("/root/Data")

@export var radius: float
var speed: float:
	get:
		return 100 / (2 * PI * radius)

@export var circle_center: Vector2
var sprite: Sprite2D
var satellite_streams = [
	preload("res://Sounds/satellite1.wav"),
	preload("res://Sounds/satellite2.wav"),
	preload("res://Sounds/satellite3.wav")
]
@onready var angle:float = deg_to_rad(90)
@onready var audio: AudioStreamPlayer2D = get_node("Audio")

# Called when the node enters the scene tree for the first time.
func _ready():
	audio.play()
	sprite = $Sprite2D
	_physics_process(0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _physics_process(delta):
	if data.isPaused():
		return
	angle += speed * delta
	var rotation = Vector2(cos(angle), sin(angle))
	var pos = (rotation * radius) + circle_center
	position = Vector2(round(pos.x), round(pos.y)) # force use int to keep pixel perfect
	if abs(position.x - circle_center.x) <= 1 || abs(position.y - circle_center.y) <= 1:
		playSound()

func recalculRadius():
	radius = position.distance_to(circle_center)
	if radius > 60:
		radius = 60
	if radius < 15:
		radius = 15
	return radius

func recalculAngle(score):
	var firstVec = position - circle_center
	var secondVec = Vector2(cos(angle), sin(angle))
	var newAngle = secondVec.angle_to(firstVec)
#	if rad_to_deg(flatAngle) <= -90:
#		flatAngle += deg_to_rad(360)
#	angle = flatAngle + deg_to_rad(score * 360)
	angle += newAngle

func playSound():
	var audioNum: int = (abs(radius) - 15) / 15
	if not audio.is_playing() && audioNum < satellite_streams.size():
		audio.stream = satellite_streams[audioNum]
		audio.play()

signal body_entered(body)

func _on_rigid_body_2d_area_entered(area):
	body_entered.emit(area)

func playSong(song):
	audio.pitch_scale = 1.5
	audio.stream = song
	audio.play()
