extends Label

var data:Data

# Called when the node enters the scene tree for the first time.
func _ready():
	data = get_node("/root/Data")
	var scores = data.getScores()
	text = "\nscores:\n\n1: " + str(scores[0]) + "\n2: " + str(scores[1]) + "\n3: " + str(scores[2])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _input(event):
	if event.is_pressed():
		get_tree().change_scene_to_file("res://Scene/menu.tscn")
