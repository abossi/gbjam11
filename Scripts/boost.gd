extends Node

@onready var boostBar = get_node("border/BoostBar") as ColorRect
var boost = 25 as int
var height = 8

func _ready():
	redraw()

func addBoost():
	boost += 1
	if boost > 50:
		boost = 50
	redraw()

func useBoost():
	if boost <= 0:
		return false
	boost -= 1
	redraw()
	return true

func redraw():
	boostBar.set_size(Vector2(boost, height))
