extends Node2D

var data:Data
@onready var texts = [
	"Houston,\nwe have a\nproblem!",
	"There are more\nand more\nsatellites.",
	"NO MORE SPACE\nIN SPACE!!!", # IT’S TOO CRAMPED UP THERE # IT IS TOO CRAMPED UP IN SPACE
	"You need to\ndrive the ship\nto stay alive!",
	"Press 'A'\nto lose altitude.",
	"Perfect!\nNow press 'B'\nto go up!",
	"When we are\nclose to the sun,\nour boost reload!",
	"Now, our boost\nis full. Use\narrows to move.",
	"Perfect!\nGood luck!"
]
var textAction = [0,
	0,
	0,
	0,
	1,
	2,
	0,
	3,
	0
]
var textRadius = [-68,
	-68,
	-68,
	-68,
	-68,
	-35,
	2,
	47,
	70,
]
@onready var i = 0
@onready var textArea:Label = get_node("ColorRect/ColorRect/textTuto")
@onready var area = get_node("ColorRect")
@onready var player = get_tree().root.get_node("game/Player")

func _ready():
	data = get_node("/root/Data")
	area.visible = false
	if not data.needTutorial():
		data.play()
		return
	await get_tree().create_timer(1.5).timeout
	data.pause()

func _process(delta):
	if not data.needTutorial():
		return
	if i < texts.size():
		if player.get_angle() >= textRadius[i]:
			textArea.text = texts[i]
			area.visible = true
			data.pause()
		else:
			area.visible = false
			data.play()
	elif area.visible:
		print("tuto done")
		data.tutorialDone()
		area.visible = false
		data.play()

func _input(event):
	if i >= texts.size() || not area.visible:
		return
	elif event.is_action_pressed("start") && textAction[i] == 0:
		i += 1
	elif event.is_action_pressed("A") && (textAction[i] == 1 || textAction[i] == 0):
		i += 1
	elif event.is_action_pressed("B") && (textAction[i] == 2 || textAction[i] == 0):
		i += 1
	elif event.is_action_pressed("down") && (textAction[i] == 3 || textAction[i] == 0):
		i += 1
		data.arrowActive()
	elif event.is_action_pressed("up") && (textAction[i] == 3 || textAction[i] == 0):
		i += 1
		data.arrowActive()
	elif event.is_action_pressed("left") && (textAction[i] == 3 || textAction[i] == 0):
		i += 1
		data.arrowActive()
	elif event.is_action_pressed("right") && (textAction[i] == 3 || textAction[i] == 0):
		i += 1
		data.arrowActive()
