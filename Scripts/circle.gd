extends Node2D

func _on_radius_changed(radius):
	set_meta("radius", radius)
	queue_redraw()


func _draw():
	if (get_meta("isFull")):
		draw_circle_arc(get_meta("radius"), 0, 360, get_meta("color"))
	else:
		draw_circle_arc(get_meta("radius"), 60, 360, get_meta("color"))
	rotate(deg_to_rad(-120))
	
func draw_circle_arc(radius, angle_from, angle_to, color):
	var nb_points = 32
	var points_arc = PackedVector2Array()

	for i in range(nb_points + 1):
		var angle_point = deg_to_rad(angle_from + i * (angle_to-angle_from) / nb_points - 90)
		points_arc.push_back(Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color)

