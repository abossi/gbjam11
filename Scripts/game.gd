extends Node2D

var i = 0
var data: Data
var satellite
var nextPop = -70
var orbit = {
	max = 15,
	min = 60,
}
@onready var player = get_node("Player")
@onready var gameOverPanel = get_node("GameOver")

@onready var menuPanel = get_node("Menu")
@onready var selectPanel = get_node("Menu/select")
var selectDelta = 19
var selectPos = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	player.body.angle = deg_to_rad(-90)
	data = get_node("/root/Data")
	data.setMusic(0)
	satellite = preload("res://Scene/satellite.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if get_player_angle() > nextPop:
		nextPop += 360
		i += 1
#		get_tree().change_scene_to_file("res://Scene/menu.tscn")
		var instance = satellite.instantiate()
		instance.circle_center = $planet.position

		var radius = -(((i % 3) + 1) * 15 + randf_range(0, 15)) # `* +1` or `* -1` => change the direction
		instance.radius = radius

		instance.add_to_group("Satellites")
		add_child(instance)

func _input(event):
	if gameOverPanel.visible && event.is_action_pressed("start"):
		get_tree().change_scene_to_file("res://Scene/menu.tscn")
	elif event.is_action_pressed("start") && not data.needTutorial() && not menuPanel.visible:
		data.pause()
		menuPanel.visible = true
		selectPos = 0
	elif menuPanel.visible:
		if event.is_action_pressed("up") && selectPos == 1:
			selectPos = 0
		if event.is_action_pressed("down") && selectPos == 0:
			selectPos = 1
		if event.is_action_pressed("A") || event.is_action_pressed("start"):
			if selectPos == 0:
				data.play()
				menuPanel.visible = false
			else:
				get_tree().change_scene_to_file("res://Scene/menu.tscn")
		selectPanel.position = Vector2(0, selectPos * selectDelta)

func get_player_angle():
	if player != null:
		return player.get_angle()
	return 0

func gameOver():
	gameOverPanel.visible = true
