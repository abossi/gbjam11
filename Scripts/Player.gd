extends Node2D

@onready var body: Node = $satellite
@onready var data:Data = get_node("/root/Data")
@onready var score:Label = get_tree().get_root().get_node("game/Score")
@onready var boostInst = get_tree().get_root().get_node("game/Boost")
@onready var game = get_tree().get_root().get_node("game")

@export var circle_center: Node
var boostDelta: Vector2
@export var radius := 42:
	set(value):
		if body == null: # Before the ready
			return
		radius = value
		body.radius = value
		radius_changed.emit(body.radius)

@export var gameOverSong:AudioStream


signal radius_changed(radius)


# Called when the node enters the scene tree for the first time.
func _ready():
	body.sprite.texture = load("res://Pictures/Satellite-Layer 1.png")
	body.radius = radius
	body.circle_center = circle_center.position
	boost_charge()
	boost_use()


func boost_charge():
	while (true):
		await get_tree().create_timer(0.2).timeout
		if not data.isPaused() and body.position.x > circle_center.position.x:
			boostInst.addBoost()


func boost_use():
	while (true):
		await get_tree().create_timer(0.02).timeout
		if data.isPaused():
			continue
		boostDelta = Vector2.ZERO
		if not data.isArrowActived():
			continue
		if Input.is_action_pressed("down") && boostInst.useBoost():
			boostDelta.y += 1
		if Input.is_action_pressed("up") && boostInst.useBoost():
			boostDelta.y -= 1
		if Input.is_action_pressed("left") && boostInst.useBoost():
			boostDelta.x -= 1
		if Input.is_action_pressed("right") && boostInst.useBoost():
			boostDelta.x += 1
		body.position += boostDelta
		if boostDelta != Vector2.ZERO:
			var s:int = (get_angle() + 90) / 360
			radius = body.recalculRadius()
			body.recalculAngle(s)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var s:int = (get_angle() + 90) / 360
	score.text = "score:\n" + str(s)


func _input(event):
	if data.isPaused():
		return
	var has_enought_boost = true
	if has_enought_boost && event.is_action_pressed("B") && radius < 60: # TODO: hardcoded max
		radius += 1
	if has_enought_boost && event.is_action_pressed("A") && 15 < radius: # TODO: hardcoded min
		radius -= 1

func get_angle():
	return rad_to_deg(body.angle)


func _on_satellite_body_entered(bod):
	if not data.isPaused():
		print("game over")
		body.playSong(gameOverSong)
		var s:int = (get_angle() + 90) / 360
		data.setScore(s)
		data.pause()
		game.gameOver()
