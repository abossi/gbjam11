extends Sprite2D

@onready var light = preload("res://Scene/light.tscn")

var lights: Array
var rng
var yMin = [8,8,7,7,6,5,4,2]

# Called when the node enters the scene tree for the first time.
func _ready():
	rng = RandomNumberGenerator.new()
	for i in range(0, rng.randf_range(3, 8)):
		new_light_random()
	light_update()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func light_update():
	var i = 1 as int
	while (true):
		await get_tree().create_timer(0.5).timeout
		move_lights(i)
		i += 1
		if (rng.randf_range(0, 2) >= 1):
			var y = rng.randf_range(-8, 8) as int
			var newLigth = light.instantiate() as ColorRect
			newLigth.position = Vector2(0, y)
			add_child(newLigth)
			lights.append(newLigth)

func new_light_random():
	var y = rng.randf_range(-8, 8) as int
	var x = rng.randf_range(-8 + abs(y), 0) as int
	var newLigth = light.instantiate() as ColorRect
	newLigth.position = Vector2(x, y)
	add_child(newLigth)
	lights.append(newLigth)

func move_lights(delta):
	for i in range(lights.size() - 1, -1, -1):
		var l = lights[i]
		var absY = abs(l.position.y) as int
		if (delta % (absY + 1) <= 1):
			l.position.x -= 1
		if (l.position.x <= -yMin[abs(l.position.y)]):
			remove_child(l)
			lights.remove_at(i)
			l.queue_free()
