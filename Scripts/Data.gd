extends Node

var score = [0, 0, 0]
var firstMenu = true
var tutorial = true
var paused = false
var arrowActived = false


func getScores():
	return score

func setScore(newScore):
	for i in range(0, 3):
		if (score[i] <= newScore):
			var tmp_score = score[i]
			score[i] = newScore
			newScore = tmp_score

func isFirstMenu():
	if firstMenu:
		firstMenu = false
		return true
	return false

func needTutorial():
	return tutorial

func tutorialDone():
	tutorial = false

func pause():
	paused = true;

func play():
	paused = false

func isPaused():
	return paused

func arrowActive():
	arrowActived = true

func isArrowActived():
	return arrowActived

@onready var musicVal = 0
func setMusic(val):
	musicVal = val

func getMusic(val):
	return musicVal
