extends Control

@export var eventA: InputEventAction
@export var eventB: InputEventAction
@export var eventUp: InputEventAction
@export var eventDown: InputEventAction
@export var eventRight: InputEventAction
@export var eventLeft: InputEventAction
@export var eventStart: InputEventAction

# Called when the node enters the scene tree for the first time.
func _ready():
	if OS.has_feature("web_android") || OS.has_feature("web_ios"):
		visible = true
	else:
		visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_a_button_down():
	eventA.pressed = true
	Input.parse_input_event(eventA)


func _on_a_button_up():
	eventA.pressed = false
	Input.parse_input_event(eventA)


func _on_b_button_down():
	eventB.pressed = true
	Input.parse_input_event(eventB)


func _on_b_button_up():
	eventB.pressed = false
	Input.parse_input_event(eventB)


func _on_up_button_down():
	eventUp.pressed = true
	Input.parse_input_event(eventUp)


func _on_up_button_up():
	eventUp.pressed = false
	Input.parse_input_event(eventUp)


func _on_down_button_down():
	eventDown.pressed = true
	Input.parse_input_event(eventDown)


func _on_down_button_up():
	eventDown.pressed = false
	Input.parse_input_event(eventDown)


func _on_left_button_down():
	eventLeft.pressed = true
	Input.parse_input_event(eventLeft)


func _on_left_button_up():
	eventLeft.pressed = false
	Input.parse_input_event(eventLeft)


func _on_right_button_down():
	eventRight.pressed = true
	Input.parse_input_event(eventRight)


func _on_right_button_up():
	eventRight.pressed = false
	Input.parse_input_event(eventRight)


func _on_button_button_down():
	eventStart.pressed = true
	Input.parse_input_event(eventStart)


func _on_button_button_up():
	eventStart.pressed = false
	Input.parse_input_event(eventStart)
